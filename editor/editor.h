#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>

#ifndef EDITOR_H
#define EDITOR_H

typedef struct {
	FILE *fp;
	char *fn;

	bool isclosed;
	char **text;
	uint64_t numlines;
} File;

int readfile(char*);
void closebuffer(int);

// Actually an array
extern File *filebuffers;
extern uint64_t numbuffers;
#endif // EDITOR_H
