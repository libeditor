#include "editor.h"
#include <stdlib.h> // malloc(), free()

File *filebuffers = NULL;
uint64_t numbuffers = 0;

// returns an index
// we don't read 1 char at a time, because then we would have to either reallocate each line again for each character read or allocate a huge buffer
int readfile(char *fn) {
	filebuffers = malloc(sizeof(File) * 5);
	File tmp;
	uint64_t numlines = 0, linechars = 0, filechars, i, index; // Unfortunately, this does not work on files of more than 18446744073709551616 bytes
	int64_t line = 0;
	uint32_t *linelengths;
	uint32_t linelength;
	char *buf;
	char ch;

	tmp.fn = fn;
	tmp.fp = fopen(tmp.fn, "r");

	if (tmp.fp == NULL) {
		fclose(tmp.fp);
		return -1;
	}

	// definitely open
	
	// Credit to http://stackoverflow.com/a/3464656
	fseek(tmp.fp, 0, SEEK_END);
	filechars = ftell(tmp.fp);
	rewind(tmp.fp);

	buf = malloc(sizeof(char) * filechars);

	fread(buf, sizeof(char), filechars, tmp.fp);

	// count the newlines
	for (i = 0; i < filechars; ch = buf[i++]) {
		if (ch == '\n') {
			numlines++;
		}
	}
	tmp.text = malloc(sizeof(char*) * numlines);
	linelengths = malloc(sizeof(uint64_t) * numlines);

	tmp.numlines = numlines;

	// count the number of chars in each line
	for (i = 0; i < filechars; ch = buf[i++]) {
		if (ch == '\n') {
			linelengths[line] = linelength;
			linelength = 0;
			line++;	
		} else {
			linelength++;
		}
	}

	for (i = 0; i < numlines+1; i++) {
		tmp.text[i] = malloc(sizeof(char) * (linelengths[1] + 2));
	}

	line = -1;
	
	// actually read the lines out
	for (i = 0; i < filechars; ch = buf[i++]) {
		if (ch == '\n') {
			(tmp.text[line])[index] = '\0';
			index = 0;
			line++;
		} else {
			(tmp.text[line])[index] = ch;
			index++;
		}

	}
}

void closebuffer(int index) {
	if ((index >= numbuffers) || (numbuffers == 0)) {
		return;
	}

	if (!(filebuffers[index].isclosed)) {
		fclose(filebuffers[index].fp);
	}

	File *files = malloc(sizeof(File)*(numbuffers-1));

	if (index != 0) {for (int i=0; i < index; i++) {
		files[i] = filebuffers[i];
	}}

	if (index != numbuffers-1) { for (int i=index+1; i < numbuffers-1; i++) {
		files[i-1] = filebuffers[i];
	}}

	free(filebuffers);
	filebuffers = files;
	numbuffers--;
}


void closeallbuffers() {
	while (numbuffers) {
		closebuffer(0);
	}
}
